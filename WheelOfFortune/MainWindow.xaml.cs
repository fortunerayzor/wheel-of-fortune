﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WheelOfFortune
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Path> paths = new List<Path>();
        private List<Canvas> grids = new List<Canvas>();
        private Polygon triangle = new Polygon() { Fill = Brushes.Black };
        private double speed = 0d;
        private double pieAngle = 0d;
        private bool speedUp;
        private double linearAcc = 1.3d;
        private double constAcc = 0.1d;
        private double targetSpeed = 0d;
        private const double radConst = Math.PI / 180d;
        private const double constFriction = 0.02d;
        private const double linearFriction = 1.01d;
        private Random rng = new Random();
        private Stopwatch frameSW = new Stopwatch();
        private Stopwatch iFrameSW = new Stopwatch();

        public MainWindow()
        {
            InitializeComponent();
            if (!System.IO.File.Exists("options.txt"))
            {
                Application.Current.Shutdown();
                MessageBox.Show("File \"options.txt\" not found.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var options = System.IO.File.ReadAllText("options.txt").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();

            int count = options.Length;
            pieAngle = 360d / count;

            for (int i = 0; i < count; i++)
            {
                var c = new Canvas();

                var rainbowColor = new HslColorExtension() { H = (360d / count) * i, S = 50d, L = 60d };
                var path = new Path() { Stroke = Brushes.Black, Fill = new SolidColorBrush(rainbowColor.GetColor()) };
                var pathGeometry = new PathGeometry();
                var pathFigure = new PathFigure() { StartPoint = new Point(), IsClosed = true };
                var lineSegment = new LineSegment();
                var arcSegment = new ArcSegment() { IsLargeArc = pieAngle >= 180d, SweepDirection = SweepDirection.Clockwise };

                pathFigure.Segments.Add(lineSegment);
                pathFigure.Segments.Add(arcSegment);
                pathGeometry.Figures.Add(pathFigure);
                path.Data = pathGeometry;

                var tb = new TextBlock();
                tb.Text = options.ElementAt(i);
                var vb = new Viewbox() { Stretch = Stretch.Uniform };
                vb.Child = tb;

                c.RenderTransform = new RotateTransform(i * pieAngle, 0d, 0d);

                c.Children.Add(path);
                c.Children.Add(vb);
                grids.Add(c);

                canvas.Children.Add(c);
            }
            canvas.Children.Add(triangle);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ArrangeControls();
            DispatcherTimer dt = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(16.666667d) };
            dt.Tick += DispaterTimerTick;
            dt.Start();
        }

        private void DispaterTimerTick(object sender, EventArgs e)
        {
#if DEBUG
            var sb = new StringBuilder();
            iFrameSW.Stop();
            sb.AppendLine($"speed: {speed}");
            sb.AppendLine($"speedup: {speedUp}");
            sb.AppendLine($"target: {targetSpeed}");
            frameSW.Restart();
#endif
            if (speedUp || speed > 0d)
            {
                if (speedUp)
                {
                    speed = Clamp(speed * linearAcc + constAcc, 0d, targetSpeed);
                    if (speed == targetSpeed)
                        speedUp = false;
                }
                else
                {
                    speed = Clamp(speed / linearFriction - constFriction, 0d, double.MaxValue);
                }
                grids.ForEach(x => RotatePath(x, speed));
            }
#if DEBUG
            sb.AppendLine($"frame: {frameSW.ElapsedMilliseconds}ms");
            sb.AppendLine($"iframe: {iFrameSW.ElapsedMilliseconds}ms");
            debugInfo.Text = sb.ToString();
            iFrameSW.Restart();
#endif
        }

        private void ArrangeControls()
        {
            var radius = canvas.ActualWidth > canvas.ActualHeight ? canvas.ActualHeight / 2d - 20d : canvas.ActualWidth / 2d - 20d;
            var pieHalfAngle = pieAngle / 2d;
            for (int i = 0; i < grids.Count; i++)
            {
                var path = (Path)grids[i].Children[0];
                var viewBox = (Viewbox)grids[i].Children[1];
                Canvas.SetLeft(grids[i], canvas.ActualWidth / 2);
                Canvas.SetTop(grids[i], canvas.ActualHeight / 2);
                var geometry = (PathGeometry)path.Data;
                var lineSegment = (LineSegment)geometry.Figures[0].Segments[0];
                var arcSegment = (ArcSegment)geometry.Figures[0].Segments[1];
                var start = new Point(Cos(pieHalfAngle) * radius, -Sin(pieHalfAngle) * radius);
                var end = new Point(Cos(pieHalfAngle) * radius, Sin(pieHalfAngle) * radius);
                lineSegment.Point = start;
                arcSegment.Point = end;
                arcSegment.Size = new Size(radius, radius);

                Canvas.SetLeft(viewBox, radius / 2.5d);
                Canvas.SetTop(viewBox, start.Y / 2.5d);
                viewBox.Width = radius / 2.5d;
                viewBox.Height = end.Y / 2.5d - start.Y / 2.5d;
            }
            triangle.Points = new PointCollection() {
                new Point(canvas.ActualWidth / 2 + radius, canvas.ActualHeight / 2),
                new Point(canvas.ActualWidth / 2 + radius + radius / 10d, canvas.ActualHeight / 2 - radius / 20d),
                new Point(canvas.ActualWidth / 2 + radius + radius / 10d, canvas.ActualHeight / 2 + radius / 20d) };
        }

        private void canvas_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        private void canvas_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            speedUp = true;
            targetSpeed = rng.Next(30, 50);
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ArrangeControls();
        }

        private static void RotatePath(UIElement element, double angle)
        {
            var trans = (RotateTransform)element.RenderTransform;
            trans.Angle += angle;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private double Cos(double deg)
        {
            return Math.Cos(deg * radConst);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private double Sin(double deg)
        {
            return Math.Sin(deg * radConst);
        }

        private double Clamp(double d, double min, double max)
        {
            if (min > max)
                return double.NaN;
            if (d < min)
                return min;
            else if (d > max)
                return max;
            return d;
        }
    }
}